﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boss : MonoBehaviour
{

    public Weapon weapon1;
    public Weapon weapon2;
    public float maxHp=100;
    public float currentHp;

    public Character player;

    private Animator animator;

    public float dmg;

    [Header("UI References")]
    [SerializeField] private Slider healthBar;

    private bool punch;
    private bool Punch {
        get { return punch; }
        set {
            animator.SetBool("Punch", value);
            punch = value;
        }
    }

    private bool spazzata;
    public bool Spazzata {
        get { return spazzata; }
        set {
            animator.SetBool("Spazzata", value);
            spazzata = value;
        }
    }

    private bool fase2=false;
    public bool Fase2 {
        get { return fase2; }
        set {
            animator.SetBool("Fase2", value);
            fase2 = value;
        }
    }


    private bool isDead;
    public bool IsDead {
        get { return isDead; }
        set {
            animator.SetBool("IsDead", value);
            isDead = value;
        }
    }

    public void SetMitragliette(bool value) {
        animator.SetBool("Sparo", value);
        animator.SetBool("Mitragliette", value);
    }

    public void SetMitragliette2(bool value) {
        animator.SetBool("Sparo", value);
        animator.SetBool("Mitragliette2", value);
    }
    public void SetSparo(bool value) {
        animator.SetBool("Sparo", value);
    }


    public bool canAttack = true;

    private float timer=0;
    private float timer2=0;

    public float punchDelay = 2;
    public float spazzataDelay=2;

    private void Awake() {
        animator = GetComponent<Animator>();
        currentHp = maxHp;
    }



    private void Update() {
        healthBar.value = currentHp / maxHp;
        if (currentHp <= maxHp / 2) {
            Fase2 = true;
        }
        if (canAttack) {
            canAttack = false;
            var move = Random.Range(1, 4); //da 1 a 3
            if (Fase2) {
                move = Random.Range(1, 6); //da 1 a 5
            }
            Debug.Log(move);
            //deve essere regolato da un timer
            switch (move) {
                case 1://fa spazzata
                case 2:
                    StartCoroutine(DoSpazzata());
                    break;
                case 3:
                    StartCoroutine(DoPunch());
                    break;
                case 4:
                    SetMitragliette(true);
                    break;
                case 5:
                    SetMitragliette2(true);
                    break;
            }
            
        }
        

    }


    public IEnumerator DoSpazzata() {
        Spazzata=true;
        var timer = 0f;
        while (Spazzata) {
            timer += Time.deltaTime;
            if (timer > spazzataDelay) {
                timer = 0;
                Spazzata = false;
            }
            yield return null;
        }
    }

    public IEnumerator DoPunch() {
        Punch = true;
        var timer = 0f;
        while (Punch) {
            timer += Time.deltaTime;
            if (timer > punchDelay) {
                timer = 0;
                Punch = false;
            }
            yield return null;
        }
    }


    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.CompareTag("Bullet")) {
            Hurt(player.dmg);
        }
        if (other.gameObject.CompareTag("Player")) {
            player.Hurt(dmg);
        }
    }

    public void Hurt(float damage) {
        currentHp -= damage;
        if (currentHp <= 0f) {
            currentHp = 0f;
            healthBar.value = currentHp;
            IsDead = true;
        }
    }
}
