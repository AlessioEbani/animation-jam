﻿using UnityEngine;

public class Weapon : MonoBehaviour {
    public Transform firePoint;

    private void Awake() {
        if(!firePoint)
            Debug.LogError(name + " - Weapon [Awake]: Missing Fire Point reference.");
    }
}
