﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Animator))]
public class Character : MonoBehaviour {
    private Rigidbody rb;
    private Animator animator;

    [Header("Body Properties")]
    #region properties
    public float movementSpeed = 15f;
    public float jumpForce = 2f;
    public float startHealth = 10f;
    [SerializeField][ReadOnly] private float health = 0f;
    #endregion

    [Header("Ground Check")]
    #region properties
    public Transform groundCheck;
    public float groundCheckRadius = 0.2f;
    public LayerMask groundMask;
    #endregion

    [Header("Shoot Properties")]
    #region properties
    [SerializeField] private Weapon weapon;
    public GameObject projectilePrefab;
    public float shootCooldown = 0.2f;
    #endregion

    [Header("UI References")]
    [SerializeField] private Slider healthBar;

    private Vector3 movementDirection = Vector3.zero;

    private bool isGrounded = true;
    private bool canShoot = true;
    private bool victory = false;
    private bool dead = false;

    public float dmg=2;
    public Boss boss;
    private void Awake() {
        rb = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();

        if(!weapon)
            Debug.LogError(name + " - Character [Awake]: Missing Weapon reference.");
    }

    private void Start() {
        health = startHealth;

        transform.rotation = Quaternion.Euler(0, 90, 0);
    }

    private void Update() {
        if(victory || dead)
            return;

        movementDirection = Vector3.zero;

        isGrounded = Physics.CheckSphere(groundCheck.position, groundCheckRadius, groundMask);

        if(Input.GetMouseButton(0) && isGrounded)
            Shoot();

        else if(Input.GetKey(KeyCode.Space) && isGrounded)
            Jump();

        else if(Input.GetKey(KeyCode.A))
            MoveLeft();

        else if(Input.GetKey(KeyCode.D))
            MoveRight();

        else if(isGrounded)
            SetIdleAnimation();
    
        rb.MovePosition(transform.position + movementDirection * movementSpeed * Time.deltaTime);

        healthBar.value = health / startHealth;
    }

    #region Health Functions
    public void Hurt(float damage) {
        health -= damage;
        if(health <= 0f) {
            health = 0f;
            healthBar.value = health;
            Die();
        }
    }

    private void Die() {
        SetDeathAnimation();
        dead = true;
    }
    #endregion

    private void Jump() {
        SetJumpingAnimation();
        rb.velocity = Vector3.up * jumpForce;
    }

    public void Victory() {
        victory = true;
        SetVictoryDanceAnimation();
    }

    #region Shoot Functions
    private void Shoot() {
        SetShootingAnimation();
        if(canShoot) {
            GameObject obj = Instantiate(projectilePrefab, weapon.firePoint.position, transform.rotation);
            canShoot = false;
            StartCoroutine(CooldownShoot(shootCooldown));
        }
    }

    private IEnumerator CooldownShoot(float duration) {
        float passedTime = 0f;

        while(passedTime <= duration) {
            passedTime += Time.deltaTime;
            yield return null;
        }

        canShoot = true;
    }
    #endregion

    #region Movement Functions
    private void MoveRight() {
        movementDirection = Vector3.left;
        transform.rotation = Quaternion.Euler(0, -90, 0);
        SetRunAnimation();
    }

    private void MoveLeft() {
        movementDirection = Vector3.right;
        transform.rotation = Quaternion.Euler(0, 90, 0);
        SetRunAnimation();
    }
    #endregion

    #region Animations Set Function
    private void SetRunAnimation() {
        if(isGrounded) {
            animator.SetBool("Running", true);
            animator.SetBool("Shooting", false);
            animator.SetBool("Dead", false);
            animator.SetBool("Jumping", false);
            animator.SetBool("Victory Dance", false);
        }
    }

    private void SetShootingAnimation() {
        animator.SetBool("Running", false);
        animator.SetBool("Shooting", true);
        animator.SetBool("Dead", false);
        animator.SetBool("Jumping", false);
        animator.SetBool("Victory Dance", false);
    }

    private void SetIdleAnimation() {
        animator.SetBool("Running", false);
        animator.SetBool("Shooting", false);
        animator.SetBool("Dead", false);
        animator.SetBool("Jumping", false);
        animator.SetBool("Victory Dance", false);
    }

    private void SetJumpingAnimation() {
        animator.SetBool("Running", false);
        animator.SetBool("Shooting", false);
        animator.SetBool("Dead", false);
        animator.SetBool("Jumping", true);
        animator.SetBool("Victory Dance", false);
    }

    private void SetVictoryDanceAnimation() {
        animator.SetBool("Running", false);
        animator.SetBool("Shooting", false);
        animator.SetBool("Dead", false);
        animator.SetBool("Jumping", false);
        animator.SetBool("Victory Dance", true);
    }

    private void SetDeathAnimation() {
        animator.SetBool("Running", false);
        animator.SetBool("Shooting", false);
        animator.SetBool("Dead", true);
        animator.SetBool("Jumping", false);
        animator.SetBool("Victory Dance", false);
    }
    #endregion

    private void OnDrawGizmosSelected() {
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(groundCheck.position, groundCheckRadius);
    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Bullet")) {
            Hurt(boss.dmg);
        }
        if (other.CompareTag("Boss")) {
            Hurt(boss.dmg);
        }
    }

    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.CompareTag("Bullet")) {
            Hurt(boss.dmg);
        }
        if (collision.gameObject.CompareTag("Boss")) {
            Hurt(boss.dmg);
        }
    }
}
